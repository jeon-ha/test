﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARFace))]  // 요구되는 의존 컴포넌트를 자동으로 추가
public class EyeTracker : MonoBehaviour
{
    [SerializeField]
    private GameObject leftEyePrefab;
    [SerializeField]
    private GameObject rightEyePrefab;

    private GameObject leftEye;
    private GameObject rightEye;

    private ARFace arFace;

    private XRFaceSubsystem xRFaceSubsystem;

    void Awake() // 항상 Start 전에 호출되며 프리팹이 인스턴스화 된 직후에 호출됨 (게임오브젝트가 시작동안 비활성화인 경우 호출되지 않음)
    {
        arFace = GetComponent<ARFace>();
    }

    private void OnEnable()
    {
        ARFaceManager faceManager = FindObjectOfType<ARFaceManager>();
        if(faceManager != null && faceManager.subsystem != null && faceManager.subsystem.SubsystemDescriptor.supportsEyeTracking) // facemanager가 참조를 가지고 있고, eyetracking을 할 경우
        {
            arFace.updated += OnUpdated;
            Debug.Log("Eye Tracking is supported");
        }
        else
        {
            Debug.LogError("Eye Tracking is not supported on this device");
        }
    }

    void OnDisable()
    {
        arFace.updated -= OnUpdated;
        SetVisibility(false);
    }

    void OnUpdated(ARFaceUpdatedEventArgs eventArgs)
    {
        if(arFace.leftEye != null && leftEye == null) // 왼쪽눈 추적중
        {
            leftEye = Instantiate(leftEyePrefab, arFace.leftEye);
            leftEye.SetActive(false);
        }

        if (arFace.rightEye != null && rightEye == null) // 오른쪽눈 추적중
        {
            rightEye = Instantiate(rightEyePrefab, arFace.rightEye);
            rightEye.SetActive(false);
        }

        // set visibility
        bool shouldBeVisible = (arFace.trackingState == TrackingState.Tracking) && (ARSession.state > ARSessionState.Ready); // arFace가 추적중이고 ARSession이 준비됐을 때
        SetVisibility(shouldBeVisible);
    }

    void SetVisibility(bool isVisible)
    {
        if(leftEye != null && rightEye != null)
        {
            leftEye.SetActive(isVisible);
            rightEye.SetActive(isVisible);
        }
    }

    void Update()
    {
        
    }
}

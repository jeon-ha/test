﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

public class EyeTrackerSupported : MonoBehaviour
{
    [SerializeField]
    private Text eyeTrackerSupportedText;

    private void OnEnable()
    {
        ARFaceManager faceManager = FindObjectOfType<ARFaceManager>();
        if(faceManager != null && faceManager.subsystem != null && faceManager.subsystem.SubsystemDescriptor.supportsEyeTracking) // facemanager가 참조를 가지고 있고, eyetracking을 할 경우
        {
            eyeTrackerSupportedText.text = "Eye Tracking is supported";
        }
        else
        {
            eyeTrackerSupportedText.text = "Eye Tracking is not supported";
        }
    }
}
